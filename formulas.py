from ctypes import sizeof
import re
import sys
import itertools

class Variable:
    def __init__(self, index: int) -> None:
        self.index = index
        self.depth = 0
        self.string = f'p{self.index}'

    def __hash__(self) -> int:
        return self.string.__hash__()
    
    def __eq__(self, __o: object) -> bool:
        return self.__hash__() == __o.__hash__()

    def to_string(self) -> str:
        return self.string

    def get_depth(self) -> int:
        return self.depth

    def substitution(self, sigma: dict):
        if self.index in sigma:
            return sigma[self.index]
        else:
            return self

    def get_vars(self):
        return {self.index}

    def get_subformulas(self):
        return {self}


class ComplexFormula:
    def __init__(self, conn: str, args: list) -> None:
        self.conn = conn
        self.arity = len(args)
        self.args = args
        self.vars = set()
        self.depth = 0
        for arg in self.args:
            if arg.depth >= self.depth:
                self.depth = arg.depth + 1
            self.vars.update(arg.get_vars())

        if self.arity == 0:
            self.string = self.conn
        else:
            self.string = f'{self.conn}({self.args[0].to_string()}'
            for arg in self.args[1:]:
                self.string += f',{arg.to_string()}'
            self.string += ')'

    def __hash__(self) -> int:
        return self.to_string().__hash__()

    def __eq__(self, __o: object) -> bool:
        return self.__hash__() == __o.__hash__()

    def get_depth(self) -> int:
        return self.depth

    def to_string(self) -> str:
        if self.arity == 0:
            return self.conn
        else:
            res = f'{self.conn}({self.args[0].to_string()}'
            for arg in self.args[1:]:
                res += f',{arg.to_string()}'
            res += ')'
            return res

    def substitution(self, sigma: dict):
        if self.vars.isdisjoint(set(sigma)):
            return self
        else:
            return ComplexFormula(self.conn, [arg.substitution(sigma) for arg in self.args])

    def get_vars(self) -> set:
        return self.vars

    def get_subformulas(self) -> set:
        if self.depth == 0:
            return {self}
        else:
            res = {self}
            for arg in self.args:
                res.update(arg.get_subformulas())
            return res


class Rule:
    def __init__(self, premisses, conclusions) -> None:
        self.premisses = frozenset(premisses)
        self.conclusions = frozenset(conclusions)

    def __le__(self, other):
        return self.premisses.issubset(other.premisses) and self.conclusions.issubset(other.conclusions)

    def __eq__(self, other) -> bool:
        return self.premisses == other.premisses and self.conclusions == other.conclusions

    def __hash__(self) -> int:
        return (self.premisses,self.conclusions).__hash__()

    def get_subformulas(self) -> set:
        """Returns a set with all the subformulas of formulas present in the rule"""
        res = set()
        for form in self.premisses:
            res.update(form.get_subformulas())
        for form in self.conclusions:
            res.update(form.get_subformulas())
        return res

    def get_subpremisses(self) -> set:
        """Returns a set with all the subformulas of formulas present in the premisses"""
        res = set()
        for form in self.premisses:
            res.update(form.get_subformulas())
        return res

    def get_subconclusions(self) -> set:
        """Returns a set with all the subformulas of formulas present in the conclusions"""
        res = set()
        for form in self.conclusions:
            res.update(form.get_subformulas())
        return res

    def to_string(self) -> str:
        """Returns a string representation of the formula"""
        res = '<'
        for form in self.premisses:
            res = res + f' {form.to_string()} ,'
        res = res.strip(',') + '/'
        for form in self.conclusions:
            res = res + f' {form.to_string()} ,'
        res = res.strip(',')
        res += '>'
        return res


class Matrix:
    def __init__(self, signature: dict, domain: set, designated: set, tables: list) -> None:
        self.signature = signature
        self.domain = domain
        self.designated = designated
        self.undesignated = self.domain.difference(self.designated)
        self.tables = tables

    def __sat_given_v__(self, form, subform: list, val: dict, val_size: int) -> bool:
        if form in val:
            print(f'Testing valuation {{ ', end='')
            for key in val:
                print(f'{key.string} -> {val[key]}; ', end='')
            print('}')
            return (val[form] in self.designated)
        else:
            current = subform[val_size]
            if type(current) == Variable:
                for truth_value in self.domain:
                    val[current] = truth_value
                    if self.__sat_given_v__(form, subform, val, val_size + 1):
                        return True
                    val.popitem()
            else:
                arg_values = [val[f] for f in current.args]
                for truth_value in self.table_entry(current.conn, tuple(arg_values)):
                    val[current] = truth_value
                    if self.__sat_given_v__(form, subform, val, val_size + 1):
                        return True
                    val.popitem()
            return False

    def __val_given_v__(self, form, subform: list, val: dict, val_size: int) -> bool:
        if form in val:
            print(f'Testing valuation {{ ', end='')
            for key in val:
                print(f'{key.string} -> {val[key]}; ', end='')
            print('}')
            return (val[form] not in self.designated)
        else:
            current = subform[val_size]
            if type(current) == Variable:
                for truth_value in self.domain:
                    val[current] = truth_value
                    if self.__val_given_v__(form, subform, val, val_size + 1):
                        return False
                    val.popitem()
            else:
                arg_values = [val[f] for f in current.args]
                for truth_value in self.table_entry(current.conn, tuple(arg_values)):
                    val[current] = truth_value
                    if self.__val_given_v__(form, subform, val, val_size + 1):
                        return False
                    val.popitem()
            return True

    def __soundness_given_v__(self, rule: Rule, subform: list, v: dict, v_size: int) -> bool:
        if v_size == len(subform):
            return False

        current = subform[v_size]
        if type(current) == Variable:
            allowed_values = self.domain
        else:
            arg_values = [v[f] for f in current.args]
            allowed_values = self.table_entry(current.conn, tuple(arg_values))

        if current in rule.premisses:
            allowed_values = allowed_values.intersection(self.designated)
        if current in rule.conclusions:
            allowed_values = allowed_values.intersection(self.undesignated)

        for truth_value in allowed_values:
            v[current] = truth_value
            if not self.__soundness_given_v__(rule, subform, v, v_size + 1):
                return False
            v.popitem()

        return True

    def table_entry(self, conn: str, args: tuple):
        return self.tables[self.signature[conn][1]][args]
    
    def print(self) -> None:
        print(f'Domain: {self.domain}')
        print(f'Designated values: {self.designated}')
        for conn in self.signature:
            print(f'Table for {conn} of arity {self.signature[conn][0]}:')
            for args in self.tables[self.signature[conn][1]]:
                print(f'    {conn}{args} = {self.tables[self.signature[conn][1]][args]}')
    
    def sat(self, form) -> bool:
        subform = list(form.get_subformulas())
        subform.sort(key=formula_depth)
        return self.__sat_given_v__(form, subform, {}, 0)

    def val(self, form) -> bool:
        subform = list(form.get_subformulas())
        subform.sort(key=formula_depth)
        return self.__val_given_v__(form, subform, {}, 0)
        
    def sound_rule(self, rule: Rule) -> bool:
        subform = list(rule.get_subformulas())
        subform.sort(key=formula_depth)
        return self.__soundness_given_v__(rule, subform, {}, 0)

    def sound_set_rules(self, set_of_rules: set) -> bool:
        res = True
        for r in set_of_rules:
            print(r.to_string())
            res = res and self.sound_rule(r)
        return res

    def sound_subrules(self, rule: Rule) -> list:
        """Returns a list with all the minimal subrules of (rule) that are sound in the matrix"""
        return_list = []
        for subpremisses in powerset(rule.premisses):
            for subconclusions in powerset(rule.conclusions):
                r = Rule(subpremisses,subconclusions)
                if r != rule:
                    if self.sound_rule(r):
                        is_dilution = False
                        for sr in return_list:
                            if sr<=r:
                                is_dilution = True
                                break
                        if not is_dilution:
                            return_list.append(r)
        return return_list

    def simplify_dilutions(self, set_rules: set) -> list:
        rule_list = [[rule] for rule in set_rules]
        for entry in rule_list:
            entry.extend(self.sound_subrules(entry[0]))
        return rule_list


def powerset(iterable):
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1))

def formula_depth(form) -> int:
    return form.get_depth()

def formula_from_string(string: str):
    string = string.strip()
    if re.fullmatch('p[1-9][0-9]*', string):
        return Variable(int(string[1:]))
    else:
        part = string.partition('(')
        if part[1] == '':
            return ComplexFormula(string, [])
        else:
            conn = part[0]
            args_string = part[2][:-1]

            pairity = 0
            args_split = []
            start_index = 0
            for i in range(len(args_string)):
                if args_string[i] == '(':
                    pairity += 1
                elif args_string[i] == ')':
                    pairity -= 1
                elif args_string[i] == ',' and pairity == 0:
                    args_split.append(args_string[start_index:i].strip())
                    start_index = i + 1
            
            args_split.append(args_string[start_index:].strip())
            args = list(map(formula_from_string,args_split))
            return ComplexFormula(conn, args)

def matrix_from_file(path: str) -> Matrix:
    f = open(path, 'r')
    lines = [line for line in f.readlines() if line != '\n']
    lines_size = len(lines)
    domain = {val.strip() for val in lines[0].strip(" {}\n").split(',')}
    domain_size = len(domain)
    designated = {val.strip() for val in lines[1].strip(" {}\n").split(',')}
    signature = {}
    tables = []
    pointer = 2
    conn_index = 0
    while pointer < lines_size:
        tables.append({})
        (conn, arity) = lines[pointer].strip().split(':')
        arity = int(arity)
        signature[conn] = (arity, conn_index)
        pointer += 1
        table_size = pow(domain_size, arity)
        for i in range(table_size):
            (args_str, result_str) = lines[pointer + i].strip().split(':')
            args = tuple([arg.strip() for arg in args_str.strip(' []\n').split(',')])
            result = {val.strip() for val in result_str.strip(' {}\n').split(',')}
            tables[conn_index][args] = result
        conn_index += 1
        pointer += table_size
    return Matrix(signature, domain, designated, tables)

def rule_from_ltsy_string(s: str) -> Rule:
    parsed = s.strip()[2:-2]
    (s_conclusions, s_premisses) = parsed.split('","')

    s_premisses = s_premisses.strip()
    s_conclusions = s_conclusions.strip()

    if s_premisses == '':
        premisses = set()
    else:
        s_premisses_set = set()
        pairity = 0
        start_index = 0
        for index in range(len(s_premisses)):
            if s_premisses[index] == '(':
                pairity += 1
            elif s_premisses[index] == ')':
                pairity -= 1
            elif s_premisses[index] == ',' and pairity == 0:
                s_premisses_set.add(s_premisses[start_index:index])
                start_index = index + 1
        s_premisses_set.add(s_premisses[start_index:])
        premisses = set(map(formula_from_string, s_premisses_set))

    if s_conclusions == '':
        conclusions = set()
    else:
        s_conclusions_set = set()
        pairity = 0
        start_index = 0
        for index in range(len(s_conclusions)):
            if s_conclusions[index] == '(':
                pairity += 1
            elif s_conclusions[index] == ')':
                pairity -= 1
            elif s_conclusions[index] == ',' and pairity == 0:
                s_conclusions_set.add(s_conclusions[start_index:index])
                start_index = index + 1
        s_conclusions_set.add(s_conclusions[start_index:])
        conclusions = set(map(formula_from_string, s_conclusions_set))

    return Rule(premisses, conclusions)

def rule_from_file_string(s: str) -> Rule:
    parsed = s.strip()[2:-2]
    (s_premisses, s_conclusions) = parsed.split('","')

    s_premisses = s_premisses.strip()
    s_conclusions = s_conclusions.strip()

    if s_premisses == '':
        premisses = set()
    else:
        s_premisses_set = set()
        pairity = 0
        start_index = 0
        for index in range(len(s_premisses)):
            if s_premisses[index] == '(':
                pairity += 1
            elif s_premisses[index] == ')':
                pairity -= 1
            elif s_premisses[index] == ',' and pairity == 0:
                s_premisses_set.add(s_premisses[start_index:index])
                start_index = index + 1
        s_premisses_set.add(s_premisses[start_index:])
        premisses = set(map(formula_from_string, s_premisses_set))

    if s_conclusions == '':
        conclusions = set()
    else:
        s_conclusions_set = set()
        pairity = 0
        start_index = 0
        for index in range(len(s_conclusions)):
            if s_conclusions[index] == '(':
                pairity += 1
            elif s_conclusions[index] == ')':
                pairity -= 1
            elif s_conclusions[index] == ',' and pairity == 0:
                s_conclusions_set.add(s_conclusions[start_index:index])
                start_index = index + 1
        s_conclusions_set.add(s_conclusions[start_index:])
        conclusions = set(map(formula_from_string, s_conclusions_set))

    return Rule(premisses, conclusions)

def rules_from_ltsy(path:str) -> dict:
    f = open(path)
    lines = f.readlines()
    rules_to_numbers = dict()
    for line in lines:
        if line.startswith('-'):
            rule_number = int(line[line.find('r')+1:line.find('-',1)].strip())
            rule_string = line[line.find('['):line.find(']')+1]
            rules_to_numbers[rule_from_ltsy_string(rule_string)] = rule_number
    return rules_to_numbers

def rules_from_file(path:str) -> dict:
    f = open(path)
    lines = f.readlines()
    rules_to_numbers = dict()
    for line in lines:
        if line.startswith('-'):
            rule_number = int(line[line.find('r')+1:line.find('-',1)].strip())
            rule_string = line[line.find('['):line.find(']')+1]
            rules_to_numbers[rule_from_file_string(rule_string)] = rule_number
    return rules_to_numbers


mat = matrix_from_file('matrices/sergio.txt')
rules = rules_from_file('rules/sergio.txt')

simplified = mat.simplify_dilutions(set(rules))

for entry in simplified:
    if len(entry) > 1:
        s = f"r{rules[entry[0]]} ->"
        for rule in entry[1:]:
            s += f" {rule.to_string()} ,"
        print(s[:-1])

# print(m.sound_set_rules(r))